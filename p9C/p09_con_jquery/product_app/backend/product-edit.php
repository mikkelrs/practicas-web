<?php
    include_once __DIR__.'/database.php';
    
    $json = file_get_contents('php://input');
    $producto = json_decode($json);
    $data = array(
        'status'  => 'error',
        'message' => 'No se pudo actualizar el producto seleccionado'
    );

    $sql = "UPDATE productos SET nombre='{$producto->nombre}', marca='{$producto->marca}', modelo='{$producto->modelo}', 
            precio={$producto->precio}, detalles='{$producto->detalles}', unidades={$producto->unidades}, imagen='{$producto->imagen}' 
            WHERE id = {$producto->id} AND eliminado = 0";
	    
    $result = $conexion->query($sql);
        
    if (!$result)
        $data['message'] = "No se pudo actualizar el producto seleccionado";
        
    $data['status'] = "success";
    $data['message'] = "Producto actualizado!";

    $conexion->close();
    echo json_encode($data, JSON_PRETTY_PRINT);
?>