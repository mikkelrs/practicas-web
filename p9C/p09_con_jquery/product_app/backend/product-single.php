<?php
    include_once __DIR__.'/database.php';

    $id = $_POST['id'];
    $sql = "SELECT * FROM productos WHERE id = $id";
    $result = $conexion->query($sql);

    if(!$result)
        die('Query Error: '.mysqli_error($conexion));

    $product = array();
	while($row = $result->fetch_array(MYSQLI_ASSOC)){
        $product[] = array(
            'id' => $row['id'],
            'nombre' => $row['nombre'],
            'marca' => $row['marca'],
            'modelo' => $row['modelo'],
            'precio' => $row['precio'],
            'detalles' => $row['detalles'],
            'unidades' => $row['unidades'],
            'imagen' => $row['imagen']
        );
    }

    $result->free();
    $conexion->close();
    echo json_encode($product[0], JSON_PRETTY_PRINT);
?>