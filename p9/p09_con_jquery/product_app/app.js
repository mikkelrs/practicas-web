$(document).ready(function () {
    var baseJSON = {
        "precio": 0.0,
        "unidades": 1,
        "modelo": "XX-000",
        "marca": "NA",
        "detalles": "NA",
        "imagen": "img/default.png"
    };
    let edit = false;
    var JsonString = JSON.stringify(baseJSON, null, 2);
    $('#description').val(JsonString);

    listarProductos();

    let errores = [
        'El nombre es requerido',
        'El nombre del producto NO debe exceder los 100 caracteres',
        'Se debe elegir la marca del producto',
        'Modelo del producto requerido',
        'El modelo del producto NO debe contener mas de 25 caracteres',
        'El modelo del producto debe contener caracteres alfanumericos',
        'Se debe ingresar precio del producto',
        'Precio minimo de $100',
        'La descripcion del producto no debe sobrepasar los 250 caracteres',
        'Unidades del producto requeridas',
        'El minimo de unidades a ingresar es de 0'
    ];

    let mensaje = [];
    var alfanumericos = /^[0-9a-zA-Z ]+$/;
    const alerta = $('#alert');

    function listarProductos() {
        $.ajax({
            url: './backend/product-list.php',
            type: 'GET',
            success: function (response) {
                let productos = JSON.parse(response);

                if (Object.keys(productos).length > 0) {
                    let template = '';

                    productos.forEach(producto => {
                        let descripcion = '';
                        descripcion += '<li>precio: ' + producto.precio + '</li>';
                        descripcion += '<li>unidades: ' + producto.unidades + '</li>';
                        descripcion += '<li>modelo: ' + producto.modelo + '</li>';
                        descripcion += '<li>marca: ' + producto.marca + '</li>';
                        descripcion += '<li>detalles: ' + producto.detalles + '</li>';

                        template += `
                                <tr productId="${producto.id}">
                                    <td>${producto.id}</td>
                                    <td>
                                        <a href="#" class="product-item">${producto.nombre}</a>
                                    </td>
                                    <td><ul>${descripcion}</ul></td>
                                    <td>
                                        <button class="product-delete btn btn-danger">
                                            Eliminar
                                        </button>
                                    </td>
                                </tr>
                            `;
                    });
                    // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                    $('#products').html(template);
                }
            }
        })
    }

    $('#search').keyup(function (e) {
        let search = $('#search').val();
        $.ajax({
            url: './backend/product-search.php',
            type: 'GET',
            data: { search },
            success: function (response) {
                let productos = JSON.parse(response);
                let resultados = '';

                if (Object.keys(productos).length > 0) {
                    let template = '';

                    productos.forEach(producto => {
                        let descripcion = '';
                        descripcion += '<li>precio: ' + producto.precio + '</li>';
                        descripcion += '<li>unidades: ' + producto.unidades + '</li>';
                        descripcion += '<li>modelo: ' + producto.modelo + '</li>';
                        descripcion += '<li>marca: ' + producto.marca + '</li>';
                        descripcion += '<li>detalles: ' + producto.detalles + '</li>';

                        resultados += `<li>${producto.nombre}</li>`;

                        template += `
                                <tr productId="${producto.id}">
                                    <td>${producto.id}</td>
                                    <td>
                                        <a href="#" class="product-item">${producto.nombre}</a>
                                    </td>
                                    <td><ul>${descripcion}</ul></td>
                                    <td>
                                        <button class="product-delete btn btn-danger">
                                            Eliminar
                                        </button>
                                    </td>
                                </tr>
                            `;
                    });
                    // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                    $('#products').html(template);

                    if (search) {
                        $('#container').html(resultados);
                        $('#product-result').removeClass('d-none');
                    }
                    else {
                        $('#container').html('');
                        $('#product-result').addClass('d-none');
                    }
                }
            }
        })
    });

    $('#description').keyup(function (e) {
        try {
            const nombre = $('#name').val();
            var productoJsonString = $('#description').val();
            var finalJSON = JSON.parse(productoJsonString);
            const marca = finalJSON.marca;
            const modelo = finalJSON.modelo;
            const detalles = finalJSON.detalles;
            const precio = finalJSON.precio;
            const unidades = finalJSON.unidades;

            validateName(nombre);
            validateBrand(marca);
            validateModel(modelo);
            validatePrice(precio);
            validatePrice(precio);
            validateDetails(detalles);
            validateUnits(unidades);
            if (finalJSON.imagen === '' || finalJSON.imagen == null)
                finalJSON.imagen = 'img/producto.png';
        }
        catch (error) {
            alert('Formato JSON incorrecto');
        }

        if (mensaje.length > 0) {
            // console.log(mensaje);
            alerta.text(mensaje.join('\n'));
            alerta.removeClass('d-none');
        }
    });

    $('#product-form').submit(function (e) {
        e.preventDefault();
        const nombre = $('#name').val();
        var productoJsonString = $('#description').val();
        var finalJSON = JSON.parse(productoJsonString);
        const marca = finalJSON.marca;
        const modelo = finalJSON.modelo;
        const detalles = finalJSON.detalles;
        const precio = finalJSON.precio;
        const unidades = finalJSON.unidades;
        if (validateName(nombre) && validateBrand(marca) && validateModel(modelo) && validatePrice(precio)
            && validatePrice(precio) && validateDetails(detalles) && validateUnits(unidades)) {
            if (finalJSON.imagen === '' || finalJSON.imagen == null)
                finalJSON.imagen = 'img/producto.png';
            // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
            finalJSON['nombre'] = $('#name').val();
            finalJSON['id'] = $('#productId').val();
            // SE OBTIENE EL STRING DEL JSON FINAL
            productoJsonString = JSON.stringify(finalJSON, null, 2);
            // console.log(productoJsonString);
            let url = edit === false ? './backend/product-add.php' : './backend/product-edit.php';
            // console.log(url);
            $.post(url, productoJsonString, function (response) {
                // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                let respuesta = JSON.parse(response);
                // console.log(respuesta);
                // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
                let template_bar = '';
                template_bar += `
                                    <li style="list-style: none;">status: ${respuesta.status}</li>
                                    <li style="list-style: none;">message: ${respuesta.message}</li>
                                `;
                // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                $('#container').html(template_bar);
                // SE HACE VISIBLE LA BARRA DE ESTADO
                $('#product-result').removeClass('d-none');
                listarProductos();
            });
        }
    });

    $(document).on('click', '.product-delete', function () {
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');

        if (confirm('¿Desea eliminar al producto seleccionado?') == true) {
            $.ajax({
                url: './backend/product-delete.php',
                type: 'GET',
                data: { id },
                success: function (response) {
                    let respuesta = JSON.parse(response);
                    let template_bar = '';
                    template_bar += `
                                    <li style="list-style: none;">status: ${respuesta.status}</li>
                                    <li style="list-style: none;">message: ${respuesta.message}</li>
                                `;
                    $('#container').html(template_bar);
                    $('#product-result').removeClass('d-none');
                    listarProductos();
                }
            })
        }
    });

    $(document).on('click', '.product-item', function () {
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');

        $.post('./backend/product-single.php', { id }, function (response) {
            let producto = JSON.parse(response);
            var productoJsonString = $('#description').val();
            var productoJSON = JSON.parse(productoJsonString);

            $('#name').val(producto.nombre);
            $('#productId').val(producto.id);
            productoJSON['precio'] = producto.precio;
            productoJSON['unidades'] = producto.unidades;
            productoJSON['modelo'] = producto.modelo;
            productoJSON['marca'] = producto.marca;
            productoJSON['detalles'] = producto.detalles;
            productoJSON['imagen'] = producto.imagen;

            productoJsonString = JSON.stringify(productoJSON, null, 2);
            
            $('#description').val(productoJsonString);
            edit = true;
        });
    });

    function validateName(nombre) {
        if (mensaje.includes(errores[0]) || mensaje.includes(errores[1]))
            mensaje.pop();
        if (nombre === '' || nombre == null) {
            if (!mensaje.includes(errores[0]))
                mensaje.push(errores[0]);
            return false;
        }
        else if (nombre.length > 100) {
            if (!mensaje.includes(errores[1]))
                mensaje.push(errores[1]);
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }

    function validateBrand(marca) {
        if (mensaje.includes(errores[2]))
            mensaje.pop();
        if (marca === '' || marca == null) {
            if (!mensaje.includes(errores[2]))
                mensaje.push(errores[2]);
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }

    function validateModel(modelo) {
        if (mensaje.includes(errores[3]))
            mensaje.pop();
        if (mensaje.includes(errores[4]))
            mensaje.pop();
        if (mensaje.includes(errores[5]))
            mensaje.pop();
        if (modelo === '' || modelo == null) {
            if (!mensaje.includes(errores[3]))
                mensaje.push(errores[3]);
            return false;
        }
        else if (modelo.length > 25 || !alfanumericos.test(modelo)) {
            if (modelo.length > 25) {
                if (!mensaje.includes(errores[4]))
                    mensaje.push(errores[4]);
            }
            if (!alfanumericos.test(modelo)) {
                if (!mensaje.includes(errores[5]))
                    mensaje.push(errores[5]);
            }
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }

    function validatePrice(precio) {
        if (mensaje.includes(errores[6]) || mensaje.includes(errores[7]))
            mensaje.pop();
        if (precio === '' || precio == null) {
            if (!mensaje.includes(errores[6]))
                mensaje.push(errores[6]);
            return false;
        }
        else if (precio < 100) {
            if (!mensaje.includes(errores[7]))
                mensaje.push(errores[7]);
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }

    function validateDetails(detalles) {
        if (mensaje.includes(errores[8]))
            mensaje.pop();
        if (detalles.length > 250) {
            if (!mensaje.includes(errores[8]))
                mensaje.push(errores[8]);
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }

    function validateUnits(units) {
        if (mensaje.includes(errores[9]) || mensaje.includes(errores[10]))
            mensaje.pop();
        if (units === '' || units == null) {
            if (!mensaje.includes(errores[9]))
                mensaje.push(errores[9]);
            return false;
        }
        else if (units < 0) {
            if (!mensaje.includes(errores[10]))
                mensaje.push(errores[10]);
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }
});
