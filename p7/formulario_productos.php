<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML, 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
        <title>Registrar producto</title>
    </head>
    <body>

        <div class="container py-5" style="width:60%;">
            <h3 class="text-center pb-3">Registrar producto</h3>

            <div class="alert alert-danger d-none" role="alert" id="alert"></div>

            <form class="row g-4 py-3" id="form">
                <div class="col-md-6">
                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre">
                </div>
                <div class="col-md-6">
                    <select class="form-select" id="marca" name="marca">
                        <option value="" selected>Elegir marca</option>
                        <option value="canon">Canon</option>
                        <option value="sony">Sony</option>
                        <option value="nikon">Nikon</option>
                        <option value="fujifilm">Fujifilm</option>
                        <option value="kodak">Kodak</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <input type="text" name="modelo" class="form-control" id="modelo" placeholder="Modelo">
                </div>
                <div class="col-md-6">
                    <input type="text" name="imagen" class="form-control" id="imagen" placeholder="img/producto.png">
                </div>
                <div class="col-md-12">
                    <textarea class="form-control" id="detalles" name="detalles" rows="4" placeholder="Descripcion del producto"></textarea>
                </div>
                <div class="col-md-6">
                    <div>
                        <div class="input-group">
                            <span class="input-group-text">$</span>
                            <input type="text" class="form-control" name="precio" id="precio">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <input type="text" name="unidades" class="form-control" id="unidades" placeholder="Unidades">
                </div>
                <div class="col-auto">
                    <div>
                        <button type="submit" class="btn btn-primary" id="registrar">Registrar producto</button>
                        <button type="reset" class="btn btn-danger" id="cancelar">Cancelar registro</button>
                    </div>
                </div>
            </form>
        </div>
        
        <script src="form.js" charset="utf-8"></script>
    </body>
</html>