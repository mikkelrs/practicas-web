document.addEventListener('DOMContentLoaded', () => {
    var btnRows = document.getElementsByTagName('button');
    for(var i=0; i<btnRows.length; i++){
        btnRows[i].addEventListener('click', event => {
            var row = event.target.parentNode.parentNode;
            var data = document.getElementById(row.id).querySelectorAll(".row-data");
            var producto = [];
            data.forEach((value) => {
                if(value.innerHTML.includes('<img'))
                    producto.push(value.firstChild.getAttribute("src").trim());
                else
                    producto.push(value.innerHTML.trim());
            });
            alert(producto);
            send2form(producto);
        });
    }
});

function send2form(producto){
    var id = producto[0];
    var nombre = producto[1];
    var marca = producto[2];
    var modelo = producto[3];
    var precio = producto[4];
    var unidades = producto[5];
    var detalles = producto[6];
    var imagen = producto[7];
    var eliminado = producto[8];

    var form = document.createElement("form");

    var idIn = document.createElement("input");
    idIn.type = 'text';
    idIn.name = 'id';
    idIn.value = id;
    form.appendChild(idIn);

    var nombreIn = document.createElement("input");
    nombreIn.type = 'text';
    nombreIn.name = 'nombre';
    nombreIn.value = nombre;
    form.appendChild(nombreIn);

    var marcaIn = document.createElement("select");
    marcaIn.name = 'marca';
    form.appendChild(marcaIn);
    
    var marcaOpt = document.createElement("option");
    marcaOpt.name = 'marcaOpt';
    marcaOpt.value = marca;
    marcaIn.appendChild(marcaOpt);

    var modeloIn = document.createElement("input");
    modeloIn.type = 'text';
    modeloIn.name = 'modelo';
    modeloIn.value = modelo;
    form.appendChild(modeloIn);

    var precioIn = document.createElement("input");
    precioIn.type = 'text';
    precioIn.name = 'precio';
    precioIn.value = precio;
    form.appendChild(precioIn);

    var unidadesIn = document.createElement("input");
    unidadesIn.type = 'text';
    unidadesIn.name = 'unidades';
    unidadesIn.value = unidades;
    form.appendChild(unidadesIn);

    var detallesIn = document.createElement("textarea");
    detallesIn.name = 'detalles';
    detallesIn.value = detalles;
    form.appendChild(detallesIn);

    var imagenIn = document.createElement("input");
    imagenIn.type = 'text';
    imagenIn.name = 'imagen';
    imagenIn.value = imagen;
    form.appendChild(imagenIn);

    var eliminadoIn = document.createElement("input");
    eliminadoIn.type = 'text';
    eliminadoIn.name = 'eliminado';
    eliminadoIn.value = eliminado;
    form.appendChild(eliminadoIn);

    form.method = 'POST';
    if(document.title == 'Actualizar producto')
        form.action = 'http://localhost/practicas-web/p7/formulario_productos_v3.php';
    else
        form.action = 'http://localhost/practicas-web/p7/formulario_productos_v2.php';
    
    document.body.appendChild(form);
    form.submit();
}