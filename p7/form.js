document.addEventListener('DOMContentLoaded', () => {
    const nombre = document.getElementById('nombre');
    const marca = document.getElementById('marca');
    const modelo = document.getElementById('modelo');
    const imagen = document.getElementById('imagen');
    const detalles = document.getElementById('detalles');
    const precio = document.getElementById('precio');
    const unidades = document.getElementById('unidades');
    const alerta = document.getElementById('alert');
    const formulario = document.getElementById('form');
    
    var alfanumericos = /^[0-9a-zA-Z ]+$/;

    formulario.addEventListener('submit', (e) => {
        let errores = [
            'El nombre es requerido',
            'El nombre del producto NO debe exceder los 100 caracteres',
            'Se debe elegir la marca del producto',
            'Modelo del producto requerido',
            'El modelo del producto NO debe contener mas de 25 caracteres',
            'El modelo del producto debe contener caracteres alfanumericos',
            'Se debe ingresar precio del producto',
            'Precio minimo de $100',
            'La descripcion del producto no debe sobrepasar los 250 caracteres',
            'Unidades del producto requeridas',
            'El minimo de unidades a ingresar es de 0'
        ];
        let mensaje = [];

        validateName();
        validateBrand();
        validateModel();
        validatePrice();
        validateDetails();
        validateQuantity();
        validateImg();
        
        function validateName(){
            if(nombre.value === '' || nombre.value == null)
                mensaje.push(errores[0]);
            else if(nombre.value.length > 100)
                mensaje.push(errores[1]);
            else
                alerta.classList.add('d-none');
        }
    
        function validateBrand(){
            if(marca.value === '')
                mensaje.push(errores[2]);
            else
                alerta.classList.add('d-none');
        }
    
        function validateModel(){
            if(modelo.value === '' || modelo.value == null)
                mensaje.push(errores[3]);
            else if(modelo.value.length > 25)
                mensaje.push(errores[4]);
            else
                alerta.classList.add('d-none');
            
            if(!alfanumericos.test(modelo.value))
                mensaje.push(errores[5]);
            else
                alerta.classList.add('d-none');
        }

        function validatePrice(){
            if(precio.value === '' || precio.value == null)
                mensaje.push(errores[6]);
            else if(precio.value < 100)
                mensaje.push(errores[7]);
            else
                alerta.classList.add('d-none');
        }

        function validateDetails(){
            if(detalles.value.length > 250)
                mensaje.push(errores[8]);
            else
                alerta.classList.add('d-none');
        }

        function validateQuantity(){
            if(unidades.value === '' || unidades.value == null)
                mensaje.push(errores[9]);
            else if(unidades.value < 0)
                mensaje.push(errores[10]);
            else
                alerta.classList.add('d-none');
        }

        function validateImg(){
            if(imagen.value === '' || imagen.value == null)
                imagen.value = 'img/producto.png';
        }

        if(mensaje.length > 0){
            e.preventDefault();
            alerta.innerText = mensaje.join('\n');
            alerta.classList.remove('d-none');
        }
    });
  });