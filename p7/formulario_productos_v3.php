<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" 
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"/>
        <title>Actualizar producto</title>
    </head>
    <body>

        <div class="container py-5" style="width:60%;">
            <h3 class="text-center pb-3">Actualizar producto</h3>

            <div class="alert alert-danger d-none" role="alert" id="alert"></div>

            <form class="row g-4 py-3" id="form" method="post" action="update.php">
                <div class="col-md-6 d-none">
                    <input type="text" name="id" class="form-control" id="id" placeholder="No. Id" value="<?= !empty($_POST['id'])?$_POST['id']:$_GET['id'] ?>">
                </div>
                <div class="col-md-6">
                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="<?= !empty($_POST['nombre'])?$_POST['nombre']:$_GET['nombre'] ?>">
                </div>
                <div class="col-md-6">
                    <select class="form-select" id="marca" name="marca">
                        <option name="marcaOpt" value="<?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?>" selected><?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?></option>
                        <option value="Canon">Canon</option>
                        <option value="Sony">Sony</option>
                        <option value="Nikon">Nikon</option>
                        <option value="Fujifilm">Fujifilm</option>
                        <option value="Kodak">Kodak</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <input type="text" name="modelo" class="form-control" id="modelo" placeholder="Modelo" value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>">
                </div>
                <div class="col-md-6">
                    <input type="text" name="imagen" class="form-control" id="imagen" placeholder="img/producto.png" value="<?= !empty($_POST['imagen'])?$_POST['imagen']: 'img/producto.png' ?>">
                </div>
                <div class="col-md-12">
                    <textarea class="form-control" id="detalles" name="detalles" rows="4" placeholder="Descripcion del producto"><?= !empty($_POST['detalles'])?$_POST['detalles']:$_GET['detalles'] ?></textarea>
                </div>
                <div class="col-md-6">
                    <div>
                        <div class="input-group">
                            <span class="input-group-text">$</span>
                            <input type="text" class="form-control" name="precio" id="precio" value="<?= !empty($_POST['precio'])?$_POST['precio']:$_GET['precio'] ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <input type="text" name="unidades" class="form-control" id="unidades" placeholder="Unidades" value="<?= !empty($_POST['unidades'])?$_POST['unidades']:$_GET['unidades'] ?>">
                </div>
                <div class="col-md-6 d-none">
                    <input type="text" name="eliminado" class="form-control" id="eliminado" placeholder="Eliminado?" value="<?= !empty($_POST['eliminado'])?$_POST['eliminado']:$_GET['eliminado'] ?>">
                </div>
                <div class="col-auto">
                    <div>
                        <button type="submit" class="btn btn-primary" id="registrar">Actualizar producto</button>
                        <button type="reset" class="btn btn-danger" id="cancelar">Cancelar</button>
                    </div>
                </div>
            </form>
        </div>
        
        <script src="form.js" charset="utf-8"></script>
    </body>
</html>