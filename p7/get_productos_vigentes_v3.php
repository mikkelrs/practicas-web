<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"/>
        <script src="postForm.js" charset="utf-8"></script>
		<title>Actualizar producto</title>
	</head>
	<body>
        <?php
            if(isset($_GET['eliminado']))
                $eliminado = $_GET['eliminado'];
            else
                die('Parámetro "eliminado" no definido...');

            if(isset($eliminado)) {
                @$link = new mysqli('localhost', 'root', 'P@$$w0rd', 'marketzone');	

                if($link->connect_errno)
                    die('Falló la conexión: '. $link->connect_error . '<br/>');
        ?>
                <?php if($result = $link->query("SELECT * FROM productos WHERE eliminado = $eliminado")): ?>
                    <table class="table table-hover">
                        <thead class="table-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Marca</th>
                                <th scope="col">Modelo</th>
                                <th scope="col">Precio</th>
                                <th scope="col">Unidades</th>
                                <th scope="col">Detalles</th>
                                <th scope="col">Imagen</th>
                                <th scope="col">Eliminado</th>
                                <th scope="col">Modificar</th>
                            </tr>
                        </thead>
                    <tbody>
                    <?php while($row = $result->fetch_array(MYSQLI_ASSOC)): ?>
                        <tr id="<?= $row['id'] ?>">
                            <th class="row-data" scope="row"><?= $row['id'] ?></th>
                            <td class="row-data"><?= $row['nombre'] ?></td>
                            <td class="row-data"><?= $row['marca'] ?></td>
                            <td class="row-data"><?= $row['modelo'] ?></td>
                            <td class="row-data"><?= $row['precio'] ?></td>
                            <td class="row-data"><?= $row['unidades'] ?></td>
                            <td class="row-data"><?= $row['detalles'] ?></td>
                            <td class="row-data"><img style="width:50%;" src="<?= $row['imagen'] ?>"/></td>
                            <td class="row-data"><?= $row['eliminado'] ?></td>
                            <td><button class="btn btn-primary" type="button">Modificar</button></td>
                        </tr>
                    <?php endwhile ?>
                        </tbody>
                    </table>
                    <?= $result->free() ?>
                <?php endif ?>
                <?php $link->close() ?>
            <?php } ?>
	</body>
</html>