<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        require_once __DIR__ . '/Pagina.php';

        $page = new Pagina('El rincon del programador', 'El sotano del programador');

        for($i=0; $i<15; $i++){
            $page->insertar_cuerpo('prueba No.'. $i . ' de la pagina');
        }

        $page->graficar();
    ?>
</body>
</html>