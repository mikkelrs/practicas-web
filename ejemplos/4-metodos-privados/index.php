<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        use EJEMPLOS\POO\Cabecera2 as Cabecera;
        require_once __DIR__ . '/Tabla.php';

        $tabla1 = new Tabla(2, 3, 'border: 1px solid');
        $tabla1->cargar(0, 0, 'A');
        $tabla1->cargar(0, 1, 'B');
        $tabla1->cargar(0, 2, 'C');
        $tabla1->cargar(1, 0, 'D');
        $tabla1->cargar(1, 1, 'E');
        $tabla1->cargar(1, 2, 'F');
        $tabla1->graficar();
    ?>
</body>
</html>