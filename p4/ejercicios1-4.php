<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML, 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <title>Practica 4 - Ejercicios 1-4</title>
    </head>
    <body>
        <?php
            include 'funciones.php';

            echo "<h1>Multiplos de 5 y 7</h1>"; // Ejercicio 1

            $num1 = $_GET['num1'];

            if(multiplo5y7($num1))
                echo $num1 . " es multiplo de 5 y 7";
            else
                echo $num1 . " no es multiplo de 5 y 7";
        ?>

        <?php
            echo "<h1>Random numbers</h1>"; // Ejercicio 2
            $matriz = random_num();
            echo "<br>";
            print_r($matriz);
            echo "<br>";
            echo count($matriz,1)-count($matriz) . " numeros generados en " . count($matriz) . " iteraciones";
        ?>

        <?php
            $num1 = $_GET['num1'];
            echo "<h1>Looking for a multiple (" . $num1 . ") of a random number</h1>"; // Ejercicio 3
            echo "<h3>While</h3>";
            multipleOf($num1);
            echo "<h3>Do-while</h3>";
            multiploDe($num1);
        ?>

        <?php
            echo "<h1>Alphabet table</h1>"; // Ejercicio 4
            alphabet_table();
        ?>
    </body>
</html>