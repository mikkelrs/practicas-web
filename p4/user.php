<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML, 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <title>Ejercicio 5</title>
    </head>
    <body>

        <div class="container px-5 py-5">
            <h3 class="text-center">Ingrese sus datos</h3>
            <form method="post" action="access.php">
                <div class="mb-3">
                    <label for="edad" class="form-label">Edad</label>
                    <input type="text" name="edad" class="form-control" required>
                </div>
                <div class="mb-4">
                    <label for="sexo" class="form-label">Sexo</label>
                    <select name="sexo" class="form-select" required>
                        <option value="" selected disabled hidden></option>
                        <option value="masculino">Masculino</option>
                        <option value="femenino">Femenino</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>
        </div>

    </body>
</html>