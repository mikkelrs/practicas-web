<?php
    $vehiculos = [
        'AAA0001' => [
            'Auto' => [
                'marca' => 'Volkswagen',
                'modelo' => 'Jetta A3 1992',
                'tipo' => 'Sedan'
            ],
            'Propietario' => [
                'nombre' => 'Laia Miranda',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Blvd. Hermanos Serdán 231, Ampliación Aquiles Serdán, Aquiles Serdán'
            ]
        ],

        'AAA0002' => [
            'Auto' => [
                'marca' => 'Volkswagen',
                'modelo' => 'Golf GTI 1984',
                'tipo' => 'Sedan'
            ],
            'Propietario' => [
                'nombre' => 'Antonio Luis Tapia',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Blvd. Hermanos Serdán 231, Ampliación Aquiles Serdán, Aquiles Serdán'
            ]
        ],

        'AAA0003' => [
            'Auto' => [
                'marca' => 'Volkswagen',
                'modelo' => 'Golf GTI 1992',
                'tipo' => 'Sedan'
            ],
            'Propietario' => [
                'nombre' => 'Miguel Reyes',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Blvd. Hermanos Serdán 231, Ampliación Aquiles Serdán, Aquiles Serdán'
            ]
        ],

        'AAA0004' => [
            'Auto' => [
                'marca' => 'Mini',
                'modelo' => 'Copper 2014',
                'tipo' => 'Hatchback'
            ],
            'Propietario' => [
                'nombre' => 'Juan Gabriel Egea',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Blvd. Atlixcáyotl Col No. 5316, Reserva Territorial Atlixcáyotl'
            ]
        ],

        'AAA0005' => [
            'Auto' => [
                'marca' => 'Seat',
                'modelo' => 'Leon 2020',
                'tipo' => 'Sedan'
            ],
            'Propietario' => [
                'nombre' => 'Rosario Plaza',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Mártires del 2 de Octubre 3915, San Baltazar Campeche'
            ]
        ],

        'AAA0006' => [
            'Auto' => [
                'marca' => 'Cupra',
                'modelo' => 'Leon 2021',
                'tipo' => 'Sedan'
            ],
            'Propietario' => [
                'nombre' => 'Borja Luna',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Av. Juarez 2512, La Paz'
            ]
        ],

        'AAA0007' => [
            'Auto' => [
                'marca' => 'Seat',
                'modelo' => 'Ibiza 1984',
                'tipo' => 'Hatchback'
            ],
            'Propietario' => [
                'nombre' => 'Micaela Vilches',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Mártires del 2 de Octubre 3915, San Baltazar Campeche'
            ]
        ],

        'AAA0008' => [
            'Auto' => [
                'marca' => 'Seat',
                'modelo' => 'Ibiza 2017',
                'tipo' => 'Hatchback'
            ],
            'Propietario' => [
                'nombre' => 'Delia Carballo',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Mártires del 2 de Octubre 3915, San Baltazar Campeche'
            ]
        ],

        'AAA0009' => [
            'Auto' => [
                'marca' => 'Mazda',
                'modelo' => 'II 2020',
                'tipo' => 'Sedan'
            ],
            'Propietario' => [
                'nombre' => 'Fabio Raya',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Blvd. Vía Atlixcayotl No. 5508-A, Reserva Territorial Atlixcáyotl'
            ]
        ],

        'AAA0010' => [
            'Auto' => [
                'marca' => 'Mazda',
                'modelo' => 'III 2021',
                'tipo' => 'Sedan'
            ],
            'Propietario' => [
                'nombre' => 'Minerva del Campo',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => '5Blvd. Vía Atlixcayotl No. 5508-A, Reserva Territorial Atlixcáyotl'
            ]
        ],

        'AAA0011' => [
            'Auto' => [
                'marca' => 'Mazda',
                'modelo' => 'CX-5 2021',
                'tipo' => 'SUV'
            ],
            'Propietario' => [
                'nombre' => 'Gaspar Julian',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Blvd. Vía Atlixcayotl No. 5508-A, Reserva Territorial Atlixcáyotl'
            ]
        ],

        'AAA0012' => [
            'Auto' => [
                'marca' => 'Suzuki',
                'modelo' => 'Vitara 2018',
                'tipo' => 'SUV'
            ],
            'Propietario' => [
                'nombre' => 'Nadia Torralba',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Blvd. Hermanos Serdán 285, Ampliación Aquiles Serdán, Aquiles Serdán'
            ]
        ],

        'AAA0013' => [
            'Auto' => [
                'marca' => 'Seat',
                'modelo' => 'Arona 2021',
                'tipo' => 'SUV'
            ],
            'Propietario' => [
                'nombre' => 'Patricia Silvestre',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Mártires del 2 de Octubre 3915, San Baltazar Campeche'
            ]
        ],

        'AAA0014' => [
            'Auto' => [
                'marca' => 'Toyota',
                'modelo' => 'RAV4 2021',
                'tipo' => 'SUV'
            ],
            'Propietario' => [
                'nombre' => 'Roger Quintero',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Atlixcáyotl 5502, Reserva Territorial Atlixcáyotl, Corredor Comercial Desarrollo'
            ]
        ],

        'AAA0015' => [
            'Auto' => [
                'marca' => 'Volkswagen',
                'modelo' => 'T-Cross 2021',
                'tipo' => 'SUV'
            ],
            'Propietario' => [
                'nombre' => 'Noel Villegas',
                'ciudad' => 'Puebla, Pue.',
                'direccion' => 'Blvd. Hermanos Serdán 231, Ampliación Aquiles Serdán, Aquiles Serdán'
            ]
        ]
    ];

    $matricula = $_POST['matricula'];
        if(array_key_exists($matricula, $vehiculos)){
            echo "Informacion del vehiculo de matricula $matricula<br><pre>";
            print_r($vehiculos[$matricula]);
            echo('</pre>');
        }
        else
            echo "La matricula es incorrecta";
?>