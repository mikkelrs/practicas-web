<?php
    function multiplo5y7($num){
        if($num % 5 == 0 && $num % 7 == 0)
            return true;
        else
            return false;
    }

    function random_num($iteracion = 1) {
        global $matriz; // Matriz global para poder guardar los valores en cada iteracion
        $num1 = rand(100,999); // Se genera un valor de 3 cifras (entre 100 y 999)
        $num2 = rand(100,999);
        $num3 = rand(100,999);
    
        $matriz[$iteracion-1][0] = $num1; // Se almacenan los valores en la matriz
        $matriz[$iteracion-1][1] = $num2;
        $matriz[$iteracion-1][2] = $num3;
        echo $num1 . ", " . $num2 . ", " . $num3 . "<br>";

        if($num1 % 2 == 1 && $num2 % 2 == 0 && $num3 % 2 == 1) // Secuencia impar, par, impar
            echo "Secuencia generada" . " en " . $iteracion . " iteraciones";
        else
            random_num($iteracion+1); // Si no se cumple la secuencia, se hace recursion en la funcion
        return $matriz;
    }

    function multipleOf($num){
        $random = rand(100,999);
        while($random % $num != 0){
            $random = rand(100,999);
            echo $random . "<br>";
        }
        echo $random . " -> multiplo de " . $num;
    }

    function multiploDe($num){
        do{
            $random = rand(100,999);
            echo $random . "<br>";
        }while($random % $num != 0);
        echo $random . " -> multiplo de " . $num;
    }

    function alphabet_table(){
        $alphabet = array();
        for($i = 97; $i <= 122; $i++)
            $alphabet[$i] = chr($i);
        echo '<div class="container px-5">
            <table class="table table-sm table-light">
                <thead>
                    <tr>
                        <th class="text-center" scope="col">Indice</th>
                        <th class="text-center" scope="col">Letra</th>
                    </tr>
                </thead>
                <tbody>';
        foreach($alphabet as $index => $letter){
            echo '<tr>
                    <td class="text-center">' . $index . '</td>
                    <td class="text-center">' . $letter . '</td>
                  </tr>';
        }
        echo '</tbody></table></div>';
    }
?>