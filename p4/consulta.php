<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML, 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <title>Consulta de vehiculos</title>
    </head>
    <body>

        <div class="container px-5 py-5">
            <h3 class="text-center">Ingrese la matricula del vehiculo</h3>
            <form method="post" action="vehiculos.php">
                <div class="py-3">
                    <label for="matricula" class="form-label">Matricula</label>
                    <input type="text" name="matricula" placeholder="LLLNNNN" class="form-control" required>
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </form>
        </div>

    </body>
</html>