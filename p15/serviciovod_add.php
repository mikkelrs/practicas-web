<?php
    libxml_use_internal_errors(true);
    $xml = new DOMDocument();
    $xml->load('./catalogovodN.xml', LIBXML_NOBLANKS);
    $xsd = './catalogovod.xsd';

    if(!$xml->schemaValidate($xsd)){
        $errors = libxml_get_errors();
        $noError = 1;
        $lista = '';

        foreach($errors as $error)
            $lista = $lista . '[' . ($noError++) . ']: ' . $error->message . ' ';
        
        echo $lista;
    }
    else{
        $usuario = $_POST['usuario'];
        $idioma = $_POST['idioma'];
        $pelGenero = $_POST['pel-genero'];
        $pelTitulo1 = $_POST['pel-titulo1'];
        $pelDuracion1 = $_POST['pel-duracion1'];
        $pelTitulo2 = $_POST['pel-titulo2'];
        $pelDuracion2 = $_POST['pel-duracion2'];

        $serGenero = $_POST['ser-genero'];
        $serTitulo1 = $_POST['ser-titulo1'];
        $serDuracion1 = $_POST['ser-duracion1'];
        $serTitulo2 = $_POST['ser-titulo2'];
        $serDuracion2 = $_POST['ser-duracion2'];

        //Creando un nodo perfil con sus atributos
        $profiles = $xml->getElementsByTagName('perfiles')[0];
        $newProfile = $profiles->appendChild($xml->createElement('perfil'));
        $newProfile->setAttribute('usuario', $usuario);
        $newProfile->setAttribute('idioma', $idioma);

        //Creando un nuevo genero de peliculas
        $movies = $xml->getElementsByTagName('peliculas')[0];
        $newMovieGenre = $movies->appendChild($xml->createElement('genero'));
        $newMovieGenre->setAttribute('nombre', $pelGenero);
        
        //Creando los titulos de las peliculas
        $newTitle = $newMovieGenre->appendChild($xml->createElement('titulo'));
        $newTitle->appendChild($xml->createTextNode($pelTitulo1));
        $newTitle->setAttribute('duracion', $pelDuracion1);
        
        $newTitle2 = $newMovieGenre->appendChild($xml->createElement('titulo'));
        $newTitle2->appendChild($xml->createTextNode($pelTitulo2));
        $newTitle2->setAttribute('duracion', $pelDuracion2);

        //Creando un nuevo genero de series
        $series = $xml->getElementsByTagName('series')[0];
        $newSeriesGenre = $series->appendChild($xml->createElement('genero'));
        $newSeriesGenre->setAttribute('nombree', $serGenero);
        
        //Creando los titulos de las series
        $newTitle3 = $newSeriesGenre->appendChild($xml->createElement('titulo'));
        $newTitle3->appendChild($xml->createTextNode($serTitulo1));
        $newTitle3->setAttribute('duracion', $serDuracion1);
        
        $newTitle4 = $newSeriesGenre->appendChild($xml->createElement('titulo'));
        $newTitle4->appendChild($xml->createTextNode($serTitulo2));
        $newTitle4->setAttribute('duracion', $serDuracion2);

        $xml->save('catalogovod2.xml');
        // echo $xml->saveXML();
    }
?>