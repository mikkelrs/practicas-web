<?php
    libxml_use_internal_errors(true);
    $xml = new DOMDocument();
    $xml->load('./catalogovodN.xml', LIBXML_NOBLANKS);
    $xsd = './catalogovod.xsd';
    $html = new DOMDocument();

    if(!$xml->schemaValidate($xsd)){
        $errors = libxml_get_errors();
        $noError = 1;
        $lista = '';

        foreach($errors as $error)
            $lista = $lista . '[' . ($noError++) . ']: ' . $error->message . ' ';
        
        echo $lista;
    }
    else{
        $perfiles = $xml->getElementsByTagName('perfil');
        $usuario = $perfiles[0]->getAttribute('usuario');
        
        $peliculas = $xml->getElementsByTagName('peliculas');
        $series = $xml->getElementsByTagName('series');
        
        $region = $peliculas[0]->getAttribute('region');
        $region2 = $series[0]->getAttribute('region');
        
        $generos = $peliculas[0]->childNodes;
        $generos2 = $series[0]->childNodes;
?>
<html>
    <head>
        <title>Tus peliculas y series</title>
        <style type="text/css">
            body {
                margin: 20px;
                font-family: Verdana, Helvetica, sans-serif;
                font-size: 90%;}
                h1 {color: #005825;
                border-bottom: 1px solid #005825;}
                h2 {font-size: 1.2em;
                color: #4A0048;
                width: 75%
                }
            img {
                width: 10%;
            }
        </style>
    </head>
    <body>
        <img src="https://upload.wikimedia.org/wikipedia/commons/0/08/Netflix_2015_logo.svg"></img>
        <h2>Bienvenido <?php echo $usuario ?>!</h2>
        <h2>Explora tu catalogo de peliculas y series</h2>
        <table>
            <tr>
                <th>
                    <h2>Peliculas region <?php echo $region ?></h2>
                </th>
            </tr>
            <tr>
                <td><h2>Titulo</h2></td>
                <td><h2>Clasificacion</h2></td>
                <td><h2>Genero</h2></td>
                <td><h2>Duracion</h2></td>
            </tr>
            <?php foreach($generos as $genero): ?>
                <?php foreach($genero->childNodes AS $titulo): ?>
                    <tr>
                        <td><?php echo $titulo->nodeValue ?></td>
                        <td>B-15</td>
                        <td><?php echo $genero->getAttribute('nombre') ?></td>
                        <td><?php echo $titulo->getAttribute('duracion') ?></td>
                    </tr>
                <?php endforeach ?>
            <?php endforeach ?>
                    </xsl:for-each>
                </xsl:for-each>
        </table>
        <table>
            <tr>
                <th>
                    <h2>Series region <?php echo $region2 ?></h2>
                </th>
            </tr>
            <tr>
                <td><h2>Titulo</h2></td>
                <td><h2>Clasificacion</h2></td>
                <td><h2>Genero</h2></td>
                <td><h2>Capitulos</h2></td>
                <td><h2>Temporadas</h2></td>
            </tr>
            <?php foreach($generos2 as $genero): ?>
                <?php foreach($genero->childNodes AS $titulo): ?>
                    <tr>
                        <td><?php echo $titulo->nodeValue ?></td>
                        <td>B-15</td>
                        <td><?php echo $genero->getAttribute('nombre') ?></td>
                        <td><?php echo $titulo->getAttribute('duracion') ?></td>
                    </tr>
                <?php endforeach ?>
            <?php endforeach ?>
         </table>
         <?php echo $html->saveHTML() ?>
    </body>
</html>
<?php } ?>