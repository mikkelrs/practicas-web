<?php
    include_once __DIR__.'/database.php';

    function duplicated_product($data, $products){
        foreach($products as $id => $product){
            if($products[$id]['nombre'] == $data[0] && $products[$id]['eliminado'] == 0)
                return true;
        }
        return false;
    }
    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $producto = file_get_contents('php://input');
    if(!empty($producto)) {
        // SE TRANSFORMA EL STRING DEL JASON A OBJETO
        $jsonOBJ = json_decode($producto);

        $nombre = $jsonOBJ->nombre;
        $marca  = $jsonOBJ->marca;
        $modelo = $jsonOBJ->modelo;
        $precio = $jsonOBJ->precio;
        $detalles = $jsonOBJ->detalles;
        $unidades = $jsonOBJ->unidades;
        $imagen = $jsonOBJ->imagen;
        $eliminado = 0;

        $clientProduct = [$nombre, $marca, $modelo, $precio, $detalles, $unidades, $imagen, $eliminado];
        
        $insert = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}', '{$eliminado}')";
        
        $select = $conexion->query("SELECT * FROM productos");
        $dbProducts = $select->fetch_all(MYSQLI_ASSOC);

        if(!duplicated_product($clientProduct, $dbProducts)){
            $conexion->query($insert);
            echo '[SERVIDOR] Producto insertado correctamente';
        }
        else
            echo '[SERVIDOR] Error. El producto ya existe y no pudo ser insertado.';
    }
?>