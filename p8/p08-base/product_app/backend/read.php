<?php
    include_once __DIR__.'/database.php';
    // SE CREA EL ARREGLO QUE SE VA A DEVOLVER EN FORMA DE JSON
    $products = array();
    // SE VERIFICA HABER RECIBIDO EL ID
    if(isset($_POST['id'])){
        $id = $_POST['id'];
        // SE REALIZA LA QUERY DE BÚSQUEDA Y AL MISMO TIEMPO SE VALIDA SI HUBO RESULTADOS
        if($sql = $conexion->query("SELECT * FROM productos WHERE nombre LIKE '%{$id}%' 
        OR marca LIKE '%{$id}%' OR detalles LIKE '%{$id}%'")){
            // SE OBTIENEN LOS RESULTADOS
            $rows = $sql->fetch_all(MYSQLI_ASSOC);

            if(!is_null($rows)){
                // SE CODIFICAN A UTF-8 LOS DATOS Y SE MAPEAN AL ARREGLO DE RESPUESTA
                foreach($rows as $key => $row){
                    foreach($row as $column => $value)
                        $products[$key][$column] = $value;
                }
            }
			$sql->free();
		} else {
            die('Query Error: '.mysqli_error($conexion));
        }
		$conexion->close();
    } 
    
    // SE HACE LA CONVERSIÓN DE ARRAY A JSON
    echo json_encode($products, JSON_PRETTY_PRINT);
?>