// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
};

let errores = [
    'El nombre es requerido',
    'El nombre del producto NO debe exceder los 100 caracteres',
    'Se debe elegir la marca del producto',
    'Modelo del producto requerido',
    'El modelo del producto NO debe contener mas de 25 caracteres',
    'El modelo del producto debe contener caracteres alfanumericos',
    'Se debe ingresar precio del producto',
    'Precio minimo de $100',
    'La descripcion del producto no debe sobrepasar los 250 caracteres',
    'Unidades del producto requeridas',
    'El minimo de unidades a ingresar es de 0'
];

let mensaje = [];
var alfanumericos = /^[0-9a-zA-Z ]+$/;
const alerta = document.getElementById('alert');

// FUNCIÓN CALLBACK DE BOTÓN "Buscar"
function buscarID(e) {
    e.preventDefault();
    document.getElementById("productos").innerHTML = '';
    // SE OBTIENE EL ID A BUSCAR
    var id = document.getElementById('search').value;

    // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
    var client = getXMLHttpRequest();
    client.open('POST', './backend/read.php', true);
    client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    client.onreadystatechange = function () {
        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            console.log('[CLIENTE]\n' + client.responseText);
            // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
            let productos = JSON.parse(client.responseText);
            // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
            if (Object.keys(productos).length > 0) {
                for (let producto of productos) {
                    // SE CREA UNA PLANTILLA PARA CREAR LA(S) FILA(S) A INSERTAR EN EL DOCUMENTO HTML
                    let template = '';
                    template += `
                        <tr>
                            <td>${producto.id}</td>
                            <td>${producto.nombre}</td>
                            <td>
                                <ul>
                                    <li>precio: ${producto.precio}</li>
                                    <li>unidades: ${producto.unidades}</li>
                                    <li>modelo: ${producto.modelo}</li>
                                    <li>marca: ${producto.marca}</li>
                                    <li>detalles: ${producto.detalles}</li>
                                </ul>
                            </td>
                        </tr>
                    `;
                    // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                    document.getElementById("productos").innerHTML += template;
                }
            }
        }
    };
    client.send("id=" + id);
}

// FUNCIÓN CALLBACK DE BOTÓN "Agregar Producto"
function agregarProducto(e) {
    e.preventDefault();
    const nombre = document.getElementById('name').value;
    // SE OBTIENE DESDE EL FORMULARIO EL JSON A ENVIAR
    var productoJsonString = document.getElementById('description').value;
    // SE CONVIERTE EL JSON DE STRING A OBJETO
    var finalJSON = JSON.parse(productoJsonString);
    const marca = finalJSON.marca;
    const modelo = finalJSON.modelo;
    const detalles = finalJSON.detalles;
    const precio = finalJSON.precio;
    const unidades = finalJSON.unidades;
    if(validateName(nombre) && validateBrand(marca) && validateModel(modelo) 
    && validatePrice(precio) && validatePrice(precio) && validateDetails(detalles)
    && validateUnits(unidades)){
        
        if (finalJSON.imagen === '' || finalJSON.imagen == null)
            finalJSON.imagen = 'img/producto.png';
        // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
        finalJSON['nombre'] = nombre;
        // console.log(finalJSON);
        // SE OBTIENE EL STRING DEL JSON FINAL
        productoJsonString = JSON.stringify(finalJSON, null, 2);
        // console.log(productoJsonString);

        // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
        var client = getXMLHttpRequest();
        client.open('POST', './backend/create.php', true);
        client.setRequestHeader('Content-Type', "application/json;charset=UTF-8");
        client.onreadystatechange = function () {
            // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
            if (client.readyState == 4 && client.status == 200) {
                console.log(client.responseText);
            }
        };
        client.send(productoJsonString);
    }

    if (mensaje.length > 0) {
        // console.log(mensaje);
        alerta.innerText = mensaje.join('\n');
        alerta.classList.remove('d-none');
    }
}

function validateName(nombre) {
    if(mensaje.includes(errores[0]) || mensaje.includes(errores[1]))
        mensaje.pop();
    if(nombre === '' || nombre == null){
        if(!mensaje.includes(errores[0]))
            mensaje.push(errores[0]);
        return false;
    }
    else if (nombre.length > 100){
        if(!mensaje.includes(errores[1]))
            mensaje.push(errores[1]);
        return false;
    }
    else{
        alerta.classList.add('d-none');
        return true;
    }
}

function validateBrand(marca) {
    if(mensaje.includes(errores[2]))
        mensaje.pop();
    if(marca === '' || marca == null){
        if(!mensaje.includes(errores[2]))
            mensaje.push(errores[2]);
        return false;
    }
    else{
        alerta.classList.add('d-none');
        return true;
    }
}

function validateModel(modelo) {
    if(mensaje.includes(errores[3]))
        mensaje.pop();
    if(mensaje.includes(errores[4]))
        mensaje.pop();
    if(mensaje.includes(errores[5]))
        mensaje.pop();
    if(modelo === '' || modelo == null){
        if(!mensaje.includes(errores[3]))
            mensaje.push(errores[3]);
        return false;
    }
    else if (modelo.length > 25 || !alfanumericos.test(modelo)){
        if (modelo.length > 25){
            if(!mensaje.includes(errores[4]))
                mensaje.push(errores[4]);
        }
        if (!alfanumericos.test(modelo)){
            if(!mensaje.includes(errores[5]))
                mensaje.push(errores[5]);
        }
        return false;
    }
    else{
        alerta.classList.add('d-none');
        return true;
    }
}

function validatePrice(precio) {
    if(mensaje.includes(errores[6]) || mensaje.includes(errores[7]))
        mensaje.pop();
    if (precio === '' || precio == null){
        if(!mensaje.includes(errores[6]))
            mensaje.push(errores[6]);
        return false;
    }
    else if (precio < 100){
        if(!mensaje.includes(errores[7]))
            mensaje.push(errores[7]);
        return false;
    }
    else{
        alerta.classList.add('d-none');
        return true;
    }
}

function validateDetails(detalles) {
    if(mensaje.includes(errores[8]))
        mensaje.pop();
    if (detalles.length > 250){
        if(!mensaje.includes(errores[8]))
            mensaje.push(errores[8]);
        return false;
    }
    else{
        alerta.classList.add('d-none');
        return true;
    }
}

function validateUnits(units) {
    if(mensaje.includes(errores[9]) || mensaje.includes(errores[10]))
        mensaje.pop();
    if (units === '' || units == null){
        if(!mensaje.includes(errores[9]))
            mensaje.push(errores[9]);
        return false;
    }
    else if (units < 0){
        if(!mensaje.includes(errores[10]))
            mensaje.push(errores[10]);
        return false;
    }
    else{
        alerta.classList.add('d-none');
        return true;
    }
}

// SE CREA EL OBJETO DE CONEXIÓN COMPATIBLE CON EL NAVEGADOR
function getXMLHttpRequest() {
    var objetoAjax;

    try {
        objetoAjax = new XMLHttpRequest();
    } catch (err1) {
        /**
         * NOTA: Las siguientes formas de crear el objeto ya son obsoletas
         *       pero se comparten por motivos historico-académicos.
         */
        try {
            // IE7 y IE8
            objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (err2) {
            try {
                // IE5 y IE6
                objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (err3) {
                objetoAjax = false;
            }
        }
    }
    return objetoAjax;
}

function init() {
    var JsonString = JSON.stringify(baseJSON, null, 2);
    document.getElementById("description").value = JsonString;
}