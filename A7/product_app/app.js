$(document).ready(function () {

    let edit = false;

    listarProductos();

    let errores = [
        'El nombre es requerido',
        'El nombre del producto NO debe exceder los 100 caracteres',
        'Se debe elegir la marca del producto',
        'Modelo del producto requerido',
        'El modelo del producto NO debe contener mas de 25 caracteres',
        'El modelo del producto debe contener caracteres alfanumericos',
        'Se debe ingresar precio del producto',
        'Precio minimo de $100',
        'La descripcion del producto no debe sobrepasar los 250 caracteres',
        'Unidades del producto requeridas',
        'El minimo de unidades a ingresar es de 0'
    ];

    let mensaje = [];
    var alfanumericos = /^[0-9a-zA-Z ]+$/;
    let alerta = $('#alert');
    let success = $('#alert2');

    function listarProductos() {
        $.ajax({
            url: './backend/product-list.php',
            type: 'GET',
            success: function (response) {
                let productos = JSON.parse(response);

                if (Object.keys(productos).length > 0) {
                    let template = '';

                    productos.forEach(producto => {
                        let descripcion = '';
                        descripcion += '<li>precio: ' + producto.precio + '</li>';
                        descripcion += '<li>unidades: ' + producto.unidades + '</li>';
                        descripcion += '<li>modelo: ' + producto.modelo + '</li>';
                        descripcion += '<li>marca: ' + producto.marca + '</li>';
                        descripcion += '<li>detalles: ' + producto.detalles + '</li>';

                        template += `
                                <tr productId="${producto.id}">
                                    <td>${producto.id}</td>
                                    <td>
                                        <a href="#" class="product-item">${producto.nombre}</a>
                                    </td>
                                    <td><ul>${descripcion}</ul></td>
                                    <td>
                                        <button class="product-delete btn btn-danger">
                                            Eliminar
                                        </button>
                                    </td>
                                </tr>
                            `;
                    });
                    // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                    $('#products').html(template);
                }
            }
        })
    }

    $('#search').keyup(function (e) {
        let search = $('#search').val();
        $.ajax({
            url: './backend/product-search.php',
            type: 'GET',
            data: { search },
            success: function (response) {
                let productos = JSON.parse(response);
                let resultados = '';

                if (Object.keys(productos).length > 0) {
                    let template = '';

                    productos.forEach(producto => {
                        let descripcion = '';
                        descripcion += '<li>precio: ' + producto.precio + '</li>';
                        descripcion += '<li>unidades: ' + producto.unidades + '</li>';
                        descripcion += '<li>modelo: ' + producto.modelo + '</li>';
                        descripcion += '<li>marca: ' + producto.marca + '</li>';
                        descripcion += '<li>detalles: ' + producto.detalles + '</li>';

                        resultados += `<li>${producto.nombre}</li>`;

                        template += `
                                <tr productId="${producto.id}">
                                    <td>${producto.id}</td>
                                    <td>
                                        <a href="#" class="product-item">${producto.nombre}</a>
                                    </td>
                                    <td><ul>${descripcion}</ul></td>
                                    <td>
                                        <button class="product-delete btn btn-danger">
                                            Eliminar
                                        </button>
                                    </td>
                                </tr>
                            `;
                    });
                    // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                    $('#products').html(template);

                    if (search) {
                        $('#container').html(resultados);
                        $('#product-result').removeClass('d-none');
                    }
                    else {
                        $('#container').html('');
                        $('#product-result').addClass('d-none');
                    }
                }
            }
        })
    });

    $('.form-group').focusout(function (e) {
        const nombre = $('#name').val();
        const marca = $('#marca').val();
        const modelo = $('#modelo').val();
        const imagen = $('#imagen').val();
        const detalles = $('#detalles').val();
        const precio = $('#precio').val();
        const unidades = $('#unidades').val();

        validateName(nombre);
        validateBrand(marca);
        validateModel(modelo);
        validatePrice(precio);
        validateDetails(detalles);
        validateUnits(unidades);
        if (imagen === '' || imagen == null)
            $('#imagen').val('img/producto.png');

        if (mensaje.length > 0) {
            let errores = [];
            mensaje.forEach(message => {
                errores += `<li>${message}</li>`;
            });
            alerta.html(errores);
            alerta.removeClass('d-none');
        }
        else {
            success.text('Campos correctos');
            success.removeClass('d-none');
        }
    });

    $('#product-form').submit(function (e) {
        e.preventDefault();
        const id = $('#productId').val();
        const nombre = $('#name').val();
        const marca = $('#marca').val();
        const modelo = $('#modelo').val();
        const imagen = $('#imagen').val();
        const detalles = $('#detalles').val();
        const precio = $('#precio').val();
        const unidades = $('#unidades').val();
        const postData = {
            id: id,
            nombre: nombre,
            marca: marca,
            modelo: modelo,
            imagen: imagen,
            detalles: detalles,
            precio: precio,
            unidades: unidades,
        };
        // console.log(postData);
        if (validateName(nombre) && validateBrand(marca) && validateModel(modelo)
            && validatePrice(precio) && validateDetails(detalles) && validateUnits(unidades)) {
            if (imagen === '' || imagen == null)
                $('#imagen').val('img/producto.png');

            let url = edit === false ? './backend/product-add.php' : './backend/product-edit.php';
            console.log(url);
            $.post(url, postData, function (response) {
                // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
                let respuesta = JSON.parse(response);
                // console.log(respuesta);
                // SE CREA UNA PLANTILLA PARA CREAR INFORMACIÓN DE LA BARRA DE ESTADO
                let template_bar = '';
                template_bar += `
                                    <li style="list-style: none;">status: ${respuesta.status}</li>
                                    <li style="list-style: none;">message: ${respuesta.message}</li>
                                `;
                // SE INSERTA LA PLANTILLA PARA LA BARRA DE ESTADO
                $('#container').html(template_bar);
                // SE HACE VISIBLE LA BARRA DE ESTADO
                $('#product-result').removeClass('d-none');
                listarProductos();
            });
        }
        else {
            alerta.text('Campos requeridos');
            alerta.removeClass('d-none');
        }
    });

    $('#name').keyup(function (e) {
        const nombre = $('#name').val();
        // console.log(nombre);
        $.ajax({
            url: './backend/product-name.php?nombre='+nombre,
            type: 'GET',
            data: { nombre },
            success: function (response) {
                let respuesta = JSON.parse(response);
                if(respuesta.status !== undefined){
                    let template_bar = '';
                    template_bar += `
                                        <li>status: ${respuesta.status}</li>
                                        <li>message: ${respuesta.message}</li>
                                    `;
                    alerta.html(template_bar);
                    alerta.removeClass('d-none');
                }
                else
                    alerta.addClass('d-none');
                listarProductos();
            }
        })
    });

    $(document).on('click', '.product-delete', function () {
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');

        if (confirm('¿Desea eliminar al producto seleccionado?') == true) {
            $.ajax({
                url: './backend/product-delete.php',
                type: 'GET',
                data: { id },
                success: function (response) {
                    let respuesta = JSON.parse(response);
                    let template_bar = '';
                    template_bar += `
                                    <li style="list-style: none;">status: ${respuesta.status}</li>
                                    <li style="list-style: none;">message: ${respuesta.message}</li>
                                `;
                    $('#container').html(template_bar);
                    $('#product-result').removeClass('d-none');
                    listarProductos();
                }
            })
        }
    });

    $(document).on('click', '.product-item', function () {
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('productId');

        $.post('./backend/product-single.php', { id }, function (response) {
            let producto = JSON.parse(response);

            $('#name').val(producto.nombre);
            $('#productId').val(producto.id);
            $('#marca').val(producto.marca);
            $('#modelo').val(producto.modelo);
            $('#imagen').val(producto.imagen);
            $('#detalles').val(producto.detalles);
            $('#precio').val(producto.precio);
            $('#unidades').val(producto.unidades);
            edit = true;
        });
    });

    function validateName(nombre) {
        if (mensaje.includes(errores[0]) || mensaje.includes(errores[1]))
            mensaje.pop();
        if (nombre === '' || nombre == null) {
            if (!mensaje.includes(errores[0]))
                mensaje.push(errores[0]);
            return false;
        }
        else if (nombre.length > 100) {
            if (!mensaje.includes(errores[1]))
                mensaje.push(errores[1]);
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }

    function validateBrand(marca) {
        if (mensaje.includes(errores[2]))
            mensaje.pop();
        if (marca === '' || marca == null) {
            if (!mensaje.includes(errores[2]))
                mensaje.push(errores[2]);
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }

    function validateModel(modelo) {
        if (mensaje.includes(errores[3]))
            mensaje.pop();
        if (mensaje.includes(errores[4]))
            mensaje.pop();
        if (mensaje.includes(errores[5]))
            mensaje.pop();
        if (modelo === '' || modelo == null) {
            if (!mensaje.includes(errores[3]))
                mensaje.push(errores[3]);
            return false;
        }
        else if (modelo.length > 25 || !alfanumericos.test(modelo)) {
            if (modelo.length > 25) {
                if (!mensaje.includes(errores[4]))
                    mensaje.push(errores[4]);
            }
            if (!alfanumericos.test(modelo)) {
                if (!mensaje.includes(errores[5]))
                    mensaje.push(errores[5]);
            }
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }

    function validatePrice(precio) {
        if (mensaje.includes(errores[6]) || mensaje.includes(errores[7]))
            mensaje.pop();
        if (precio === '' || precio == null) {
            if (!mensaje.includes(errores[6]))
                mensaje.push(errores[6]);
            return false;
        }
        else if (precio < 100) {
            if (!mensaje.includes(errores[7]))
                mensaje.push(errores[7]);
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }

    function validateDetails(detalles) {
        if (mensaje.includes(errores[8]))
            mensaje.pop();
        if (detalles.length > 250) {
            if (!mensaje.includes(errores[8]))
                mensaje.push(errores[8]);
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }

    function validateUnits(units) {
        if (mensaje.includes(errores[9]) || mensaje.includes(errores[10]))
            mensaje.pop();
        if (units === '' || units == null) {
            if (!mensaje.includes(errores[9]))
                mensaje.push(errores[9]);
            return false;
        }
        else if (units < 0) {
            if (!mensaje.includes(errores[10]))
                mensaje.push(errores[10]);
            return false;
        }
        else {
            alerta.addClass('d-none');
            return true;
        }
    }
});
