<?php
    use API\Productos;
    include_once __DIR__.'/API/Productos.php';

    $producto = json_decode(json_encode($_POST));

    $products = new Productos();
    $products->edit($producto);
    echo $products->getResponse();
?>