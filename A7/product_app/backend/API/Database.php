<?php
    namespace API;
    abstract class Database{
        protected $conexion;

        public function __construct($database){
            $this->conexion = @mysqli_connect(
                'localhost',
                'root',
                'P@$$w0rd',
                $database
            );

            if(!$this->conexion)
                die('¡Base de datos NO conectada!');
        }
    }
?>