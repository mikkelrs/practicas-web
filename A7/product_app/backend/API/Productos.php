<?php
    namespace API;
    require_once __DIR__ . '/Database.php';
    class Productos extends Database{
        private $response = [];

        public function __construct($response = null){
            parent::__construct('marketzone');
            $this->response = $response;
        }

        public function getResponse(){
            return json_encode($this->response, JSON_PRETTY_PRINT);
        }

        public function add($product){
            $this->response = array(
                'status'  => 'error',
                'message' => 'Ya existe un producto con ese nombre'
            );
            if(isset($product)){
                $sql = "SELECT * FROM productos WHERE nombre = '{$product->nombre}' AND eliminado = 0";
                $result = $this->conexion->query($sql);
                
                if($result->num_rows == 0){
                    $this->conexion->set_charset("utf8");
                    $sql = "INSERT INTO productos VALUES (null, '{$product->nombre}', '{$product->marca}', '{$product->modelo}', {$product->precio}, '{$product->detalles}', {$product->unidades}, '{$product->imagen}', 0)";
                    if($this->conexion->query($sql)){
                        $this->response['status'] =  "success";
                        $this->response['message'] =  "Producto agregado";
                    } else {
                        $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                    }
                }
                $result->free();
                $this->conexion->close();
            }
        }

        public function delete($product){
            $this->response = array(
                'status'  => 'error',
                'message' => 'La consulta falló'
            );
            if(isset($product)){
                $sql = "UPDATE productos SET eliminado=1 WHERE id = {$product}";
                if($this->conexion->query($sql)){
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto eliminado";
                } else {
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
                $this->conexion->close();
            } 
        }

        public function edit($product){
            $this->response = array(
                'status'  => 'Error',
                'message' => 'No se pudo actualizar el producto seleccionado'
            );

            $sql = "UPDATE productos SET nombre='{$product->nombre}', marca='{$product->marca}', modelo='{$product->modelo}', 
                    precio={$product->precio}, detalles='{$product->detalles}', unidades={$product->unidades}, imagen='{$product->imagen}' 
                    WHERE id = {$product->id} AND eliminado = 0";
            $this->conexion->set_charset("utf8");
            
            if($this->conexion->query($sql)){
                $this->response['status'] = "Success";
                $this->response['message'] = "Producto actualizado!";
            }
            else
                $this->response['message'] = "No se pudo actualizar el producto seleccionado";
            $this->conexion->close();
        }

        public function list(){
            if($result = $this->conexion->query("SELECT * FROM productos WHERE eliminado = 0")) {
                $rows = $result->fetch_all(MYSQLI_ASSOC);
        
                if(!is_null($rows)){
                    foreach($rows as $num => $row){
                        foreach($row as $key => $value)
                            $this->response[$num][$key] = utf8_encode($value);
                    }
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
            $this->conexion->close();
        }

        public function search($product){
            if(isset($product)){
                $sql = "SELECT * FROM productos WHERE (id = '{$product}' OR nombre LIKE '%{$product}%' OR marca LIKE '%{$product}%' OR detalles LIKE '%{$product}%') AND eliminado = 0";
                if($result = $this->conexion->query($sql)){
                    $rows = $result->fetch_all(MYSQLI_ASSOC);
        
                    if(!is_null($rows)){
                        foreach($rows as $num => $row){
                            foreach($row as $key => $value)
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                    $result->free();
                } else {
                    die('Query Error: '.mysqli_error($this->conexion));
                }
                $this->conexion->close();
            } 
        }
        
        public function single($id){
            $sql = "SELECT * FROM productos WHERE id = $id";
            $result = $this->conexion->query($sql);
        
            if(!$result)
                die('Query Error: '.mysqli_error($this->conexion));
        
            while($row = $result->fetch_array(MYSQLI_ASSOC)){
                $this->response = array(
                    'id' => $row['id'],
                    'nombre' => $row['nombre'],
                    'marca' => $row['marca'],
                    'modelo' => $row['modelo'],
                    'precio' => $row['precio'],
                    'detalles' => $row['detalles'],
                    'unidades' => $row['unidades'],
                    'imagen' => $row['imagen']
                );
            }
            $result->free();
            $this->conexion->close();
        }
    }
?>