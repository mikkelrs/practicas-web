<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML, 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Variables en PHP</title>
    </head>
    <body>
        <?php
            echo "<h1>Ejercicio 1 - Variables validas en PHP</h1>"; // Ejercicio 1

            $_myvar = '_myvar'; // Es valido iniciar variables con guion bajo
            var_dump($_myvar);

            $_7var = '_7var';       // Tambien son validas las variables que incluyen numeros
            echo "<h2>$_7var</h2>"; // Siempre y cuando no se incluyan al inicio de la variable. 
                                    // P. ej., la siguiente variable es incorrecta: $7var = 2;

            // myvar = 3;  // Toda variable se debe inicializar con el signo de pesos ('$')
            // echo myvar; // De otra forma, sera invalida; como se observa en el ejemplo

            $myvar = 'myvar'; // Esta variable es totalmente valida, puesto que incluye letras dentro del abecedario
            print_r($myvar);

            $var7 = 'var7'; // El nombre de una variable puede incluir numeros al final de la misma
            echo "<h2>$var7</h2>";

            $_element1 = '_element1'; // Una variable valida puede iniciar con guion bajo e incluso
            echo "<h2>$_element1</h2>";          // terminar con un numero al mismo tiempo
            
            // $house*5; // Una variable no puede ser nombrada con operadores
            // echo $house*5;

            unset($_myvar, $_7var, $myvar, $var7, $_element1); // Liberando las variables del Ejercicio 1
        ?>

        <?php
            echo "<h1>Ejercicio 2 - Reasignando variables</h1>";
            echo "<h2>Valores iniciales</h2>";

            $a = "ManejadorSQL";
            $b = 'MySQL';
            $c = &$a;

            echo "<p>a = '$a'</p>
                  <p>b = '$b'</p>
                  <p>c = '$c'</p>";

            $a = "PHP Server";
            $b = &$a;
            
            echo "<h2>Valores reasignados</h2>";
            echo "<p>a) El contenido de la variable <strong>a</strong> fue reasignado por la cadena '<b>$a</b>'</p>";
            echo "<p>Por lo que ahora <strong>a</strong> = '$a'</p>";

            echo "<p>b) Mientras que la variable <strong>b</strong> ahora hace referencia a la variable 
                  <strong>a</strong>, por lo que se espera que contenga el valor de <strong>a</strong> 
                  que ahora es igual a '<b>$a</b>'</p>";
            echo "<p><strong>b</strong> = '$b'</p>";

            echo "<p>c) Por ultimo, el contenido de la variable <strong>c</strong> pasa a ser el contenido de la variable 
                  <strong>a</strong>, porque recordemos que <strong>c</strong> apunta hacia <strong>a</strong>. 
                  De esta forma, <strong>c</strong> ahora contiene '$c'</p>";
            echo "<p><strong>c</strong> = '$c'</p>";

            unset($a, $b, $c); // Liberando las variables del Ejercicio 2
        ?>

        <?php
            echo "<h1>Ejercicio 3 - Reasignando variables</h1>";
            echo "<h2>Valores iniciales</h2>";

            $a = "PHP5";
            echo "a = '$a'<br>";

            $z[] = &$a; // Arreglo z inicializado en la posicion 0 con la referencia de la variable a ('PHP5')
            var_dump($z);

            $b = "5a version de PHP";
            echo "<br>b = '$b'<br>";

            settype($b,"integer"); // Conviertiendo la variable b en entero para poder asignarla a la variable c ¿?
                                   // Ahora b vale 5. Ya que es el unico entero en la cadena.

            $c = $b*10; // c vale 50
            echo "c = '$c'<br>";

            $a .= $b; // Concatenacion de a y b (PHP5 y 5) --> PHP55
            echo "a = '$a'<br>";

            $b *= $c; // Ahora b vale 250 (b = b*c --> b = 5*50)
            echo "b = '$b'<br>";

            $z[0] = "MySQL"; // El arreglo z en la posicion 0 paso de apuntar hacia 
                             // la variable a ('PHP5') a ser igual a la cadena 'MySQL'
            var_dump($z);

            echo "<h2>Valores posteriores</h2>";
            
            echo "a = '$a'<br>";
            echo "b = '$b'<br>";
            echo "c = '$c'<br>";
            var_dump($z);

            // unset($a, $b, $c, $z); // Liberando las variables del Ejercicio 3
        ?>

        <?php
            echo "<h1>Ejercicio 4 - Variables Globales</h1>";

            echo "a = " . $GLOBALS['a'] . "<br>";
            echo "b = " . $GLOBALS['b'] . "<br>";
            echo "c = " . $GLOBALS['c'] . "<br>";
            var_dump($GLOBALS['z']);

            unset($a, $b, $c, $z); // Liberando las variables del Ejercicio 3 y 4
        ?>

        <?php
            echo "<h1>Ejercicio 5 - Script con variables</h1>";

            $a = "7 personas";
            $b = (integer) $a; // b es igual al entero contenido en la variable a
            $a = "9E3";
            $c = (double) $a; // c es igual al flotante contenido en la variable a

            var_dump($a, $b, $c);

            unset($a, $b, $c); // Liberando las variables del Ejercicio 5
        ?>

        <script language="php"> // Etiqueta removida para PHP a partir de PHP7
            echo "<h1>Ejercicio 5 - Script con variables</h1>";
        </script>

        <?php
            echo "<h1>Ejercicio 6 - Booleanos</h1>";

            $a = "0";
            $b = "TRUE";
            $c = TRUE;
            $d = ($a OR $b);
            $e = ($a AND $c);
            $f = ($a XOR $b);
            
            echo "a = ";
            var_dump($a);

            echo "<br>b = ";
            var_dump($b);

            echo "<br>c = ";
            var_dump($c);

            echo "<br>d = ";
            var_dump($d);

            echo "<br>e = ";
            var_dump($e);

            echo "<br>f = ";
            var_dump($f);

            settype($c, "int");
            settype($e, "int");
            echo "<br>Booleanos convertidos a enteros:<br>";
            echo "c = " . $c . "<br>" . "e = " . $e;

            unset($a, $b, $c, $d, $e, $f); // Liberando las variables del Ejercicio 6
        ?>

        <?php
            echo '<h1>Ejercicio 7 - Variable $_SERVER</h1>';

            echo "Version de Apache y PHP: " . $_SERVER['SERVER_SOFTWARE'];
            echo "<br>Nombre del Servidor: " . $_SERVER['SERVER_NAME'];
            echo "<br>Idioma del navegador: " . $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        ?>
    </body>
</html>