<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML, 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
        <title>Ejercicio 2</title>
    </head>
    <body>

        <div class="container py-5" style="width:60%;">
            <h3 class="text-center">Producto a registrar</h3>
            <form class="row g-4 py-5" method="post" action="set_producto_v2.php">
                <div class="col-md-6">
                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre">
                </div>
                <div class="col-md-6">
                    <input type="text" name="marca" class="form-control" id="marca" placeholder="Marca">
                </div>
                <div class="col-md-6">
                    <input type="text" name="modelo" class="form-control" id="modelo" placeholder="Modelo">
                </div>
                <div class="col-md-6">
                    <input type="text" name="imagen" class="form-control" id="imagen" placeholder="img/producto.png">
                </div>
                <div class="col-md-12">
                    <textarea class="form-control" id="detalles" name="detalles" rows="4" placeholder="Descripcion del producto"></textarea>
                </div>
                <div class="col-md-6">
                    <div>
                        <div class="input-group">
                            <span class="input-group-text">$</span>
                            <input type="text" class="form-control" name="precio" id="precio">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <input type="text" name="unidades" class="form-control" id="unidades" placeholder="Unidades">
                </div>
                <div class="col-auto">
                    <div>
                        <button type="submit" class="btn btn-primary">Registrar producto</button>
                        <button type="reset" class="btn btn-danger">Cancelar registro</button>
                    </div>
                </div>
            </form>
        </div>

    </body>
</html>