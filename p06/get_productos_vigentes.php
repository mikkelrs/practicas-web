<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Producto</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
	</head>
	<body>
        <?php
            if(isset($_GET['eliminado']))
                $eliminado = $_GET['eliminado'];
            else
                die('Parámetro "eliminado" no definido...');

            if(isset($eliminado)) {
                @$link = new mysqli('localhost', 'root', 'P@$$w0rd', 'marketzone');	

                if($link->connect_errno)
                    die('Falló la conexión: '. $link->connect_error . '<br/>');

                if($result = $link->query("SELECT * FROM productos WHERE eliminado = $eliminado")) {
                    echo '
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Marca</th>
                            <th scope="col">Modelo</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Unidades</th>
                            <th scope="col">Detalles</th>
                            <th scope="col">Imagen</th>
                            <th scope="col">Eliminado</th>
                            </tr>
                        </thead>
                    <tbody>';
                    while($row = $result->fetch_array(MYSQLI_ASSOC)){
                        echo '
                        <tr>
                            <th scope="row">' . $row['id'] . '</th>
                            <td>' . $row['nombre'] . '</td>
                            <td>' . $row['marca'] . '</td>
                            <td>' . $row['modelo'] . '</td>
                            <td>' . $row['precio'] . '</td>
                            <td>' . $row['unidades'] . '</td>
                            <td>' . utf8_encode($row['detalles']) . '</td>
                            <td><img src=' . $row['imagen'] . '></td>
                            <td>' . $row['eliminado'] . '</td>
                        </tr>';
                    }
                    echo '</tbody></table>';
                    $result->free();
                }
                $link->close();
            }
        ?>
	</body>
</html>