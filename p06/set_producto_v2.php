<?php
    $nombre = $_POST['nombre'];
    $marca  = $_POST['marca'];
    $modelo = $_POST['modelo'];
    $precio = $_POST['precio'];
    $detalles = $_POST['detalles'];
    $unidades = $_POST['unidades'];
    $imagen = $_POST['imagen'];
    $eliminado = 0;

    $datos = [$nombre, $marca, $modelo, $precio, $detalles, $unidades, $imagen, $eliminado];

    function filled_form($datos){
        foreach($datos as $dato){
            if($dato != '')
                continue;
            else
                return false;
        }
        return true;
    }

    function duplicated_product($data, $products){
        foreach($products as $id => $product){
            if($products[$id]['nombre'] == $data[0])
                return true;
        }
        return false;
    }

    @$link = new mysqli('localhost', 'root', 'P@$$w0rd', 'marketzone');	

    if ($link->connect_errno) 
        die('Falló la conexión: '.$link->connect_error.'<br/>');

    $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}', '{$eliminado}')";
    $select = $link->query("SELECT * FROM productos");
    $productos = $select->fetch_all(MYSQLI_ASSOC);

    if(filled_form($datos)){
        if(!duplicated_product($datos,$productos)){
            $link->query($sql);
            echo 'Producto de Id ' . $link->insert_id . ' insertado con los siguientes valores: <br><pre>';
            print_r($datos);
            echo('</pre>');
        }
        else
            echo '<h1>Error</h1>' . '<h3>El producto ya existe y no pudo ser insertado.</h3>';
    }
    else
        echo '<h1>Error</h1>' . '<h3>Campo o campos vacios en el formulario.</h3>';

    $link->close();
?>