<?php
    use API\Read\Read as Read;
    require_once __DIR__ . '/../../start.php';

    $products = new Read();
    $products->search($_GET['search']);
    echo $products->getResponse();
?>