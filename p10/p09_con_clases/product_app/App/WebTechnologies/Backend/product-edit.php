<?php
    use API\Update\Update as Update;
    require_once __DIR__ . '/../../start.php';

    $producto = json_decode(json_encode($_POST));

    $products = new Update();
    $products->edit($producto);
    echo $products->getResponse();
?>