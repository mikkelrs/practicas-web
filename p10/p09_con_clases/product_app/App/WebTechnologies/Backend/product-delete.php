<?php
    use API\Delete\Delete as Delete;
    require_once __DIR__ . '/../../start.php';

    $products = new Delete();
    $products->delete($_GET['id']);
    echo $products->getResponse();
?>