<?php
    use API\Create\Create as Create;
    require_once __DIR__ . '/../../start.php';

    $producto = json_decode(json_encode($_POST));

    $products = new Create();
    $products->add($producto);
    echo $products->getResponse();
?>