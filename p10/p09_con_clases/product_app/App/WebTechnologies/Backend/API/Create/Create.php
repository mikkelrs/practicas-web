<?php
    namespace API\Create;
    use API\Database;
    class Create extends Database{
        public function add($product){
            $this->response = array(
                'status'  => 'error',
                'message' => 'Ya existe un producto con ese nombre'
            );
            if(isset($product)){
                $sql = "SELECT * FROM productos WHERE nombre = '{$product->nombre}' AND eliminado = 0";
                $result = $this->conexion->query($sql);
                    
                if($result->num_rows == 0){
                    $this->conexion->set_charset("utf8");
                    $sql = "INSERT INTO productos VALUES (null, '{$product->nombre}', '{$product->marca}', '{$product->modelo}', {$product->precio}, '{$product->detalles}', {$product->unidades}, '{$product->imagen}', 0)";
                    if($this->conexion->query($sql)){
                        $this->response['status'] =  "success";
                        $this->response['message'] =  "Producto agregado";
                    } else {
                        $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                    }
                }
                $result->free();
                $this->conexion->close();
            }
        }
    }
?>