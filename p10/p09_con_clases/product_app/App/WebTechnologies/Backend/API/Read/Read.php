<?php
    namespace API\Read;
    use API\Database;
    class Read extends Database{
        public function list(){
            if($result = $this->conexion->query("SELECT * FROM productos WHERE eliminado = 0")) {
                $rows = $result->fetch_all(MYSQLI_ASSOC);
        
                if(!is_null($rows)){
                    foreach($rows as $num => $row){
                        foreach($row as $key => $value)
                            $this->response[$num][$key] = utf8_encode($value);
                    }
                }
                $result->free();
            } else {
                die('Query Error: '.mysqli_error($this->conexion));
            }
            $this->conexion->close();
        }

        public function search($product){
            if(isset($product)){
                $sql = "SELECT * FROM productos WHERE (id = '{$product}' OR nombre LIKE '%{$product}%' OR marca LIKE '%{$product}%' OR detalles LIKE '%{$product}%') AND eliminado = 0";
                if($result = $this->conexion->query($sql)){
                    $rows = $result->fetch_all(MYSQLI_ASSOC);
        
                    if(!is_null($rows)){
                        foreach($rows as $num => $row){
                            foreach($row as $key => $value)
                            $this->response[$num][$key] = utf8_encode($value);
                        }
                    }
                    $result->free();
                } else {
                    die('Query Error: '.mysqli_error($this->conexion));
                }
                $this->conexion->close();
            } 
        }
        
        public function single($id){
            $sql = "SELECT * FROM productos WHERE id = $id";
            $result = $this->conexion->query($sql);
        
            if(!$result)
                die('Query Error: '.mysqli_error($this->conexion));
        
            while($row = $result->fetch_array(MYSQLI_ASSOC)){
                $this->response = array(
                    'id' => $row['id'],
                    'nombre' => $row['nombre'],
                    'marca' => $row['marca'],
                    'modelo' => $row['modelo'],
                    'precio' => $row['precio'],
                    'detalles' => $row['detalles'],
                    'unidades' => $row['unidades'],
                    'imagen' => $row['imagen']
                );
            }
            $result->free();
            $this->conexion->close();
        }
    }
?>