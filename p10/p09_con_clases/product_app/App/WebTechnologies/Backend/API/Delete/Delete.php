<?php
    namespace API\Delete;
    use API\Database;
    class Delete extends Database{
        public function delete($product){
            $this->response = array(
                'status'  => 'error',
                'message' => 'La consulta falló'
            );
            if(isset($product)){
                $sql = "UPDATE productos SET eliminado=1 WHERE id = {$product}";
                if($this->conexion->query($sql)){
                    $this->response['status'] =  "success";
                    $this->response['message'] =  "Producto eliminado";
                } else {
                    $this->response['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
                }
                $this->conexion->close();
            } 
        }
    }
?>