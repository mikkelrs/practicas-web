<?php
    namespace API\Update;
    use API\Database;
    class Update extends Database{
        public function edit($product){
            $this->response = array(
                'status'  => 'Error',
                'message' => 'No se pudo actualizar el producto seleccionado'
            );

            $sql = "UPDATE productos SET nombre='{$product->nombre}', marca='{$product->marca}', modelo='{$product->modelo}', 
                    precio={$product->precio}, detalles='{$product->detalles}', unidades={$product->unidades}, imagen='{$product->imagen}' 
                    WHERE id = {$product->id} AND eliminado = 0";
            $this->conexion->set_charset("utf8");
            
            if($this->conexion->query($sql)){
                $this->response['status'] = "Success";
                $this->response['message'] = "Producto actualizado!";
            }
            else
                $this->response['message'] = "No se pudo actualizar el producto seleccionado";
            $this->conexion->close();
        }
    }
?>