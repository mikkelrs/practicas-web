<?php
    namespace API;
    abstract class Database{
        protected $conexion;
        protected $response = [];

        public function __construct($database = 'marketzone'){
            $this->conexion = @mysqli_connect(
                'localhost',
                'root',
                'P@$$w0rd',
                $database
            );

            $this->response = null;

            if(!$this->conexion)
                die('¡Base de datos NO conectada!');
        }

        public function getResponse(){
            return json_encode($this->response, JSON_PRETTY_PRINT);
        }
    }
?>